var express = require('express')
var app = express();
var path = require('path');
var util = require('util');

var port = process.env.PORT || 8081;

app.use(express.static(path.join(__dirname, 'game')));

app.get('/', function (req, res) {
  res.render('game/index.html')
});


app.listen(port, function(){
	console.log("app listening on", port);
});

